import {CSSReset, theme, ThemeProvider} from '@chakra-ui/core';
import axios from 'axios';
import React, {Fragment} from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import Routes from './components/routes/Routes';
import store from './state/store';
import setAuthToken from './utils/setAuthToken';

axios.defaults.baseURL = 'http://localhost:5000';

// app's theme settings
const appTheme = {
  ...theme,
};

if (localStorage.token) setAuthToken(localStorage.token);

const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={appTheme}>
        <CSSReset />
        <Router>
          <Fragment>
            <Routes />
          </Fragment>
        </Router>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
