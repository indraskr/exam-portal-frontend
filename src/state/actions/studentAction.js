import axios from 'axios';
import {GET_STUDENTS} from '../types';

export const getStudents = () => async dispatch => {
  try {
    const res = await axios.get('/api/students');
    dispatch({type: GET_STUDENTS, payload: res.data});
  } catch (err) {
    console.error(err);
  }
};
