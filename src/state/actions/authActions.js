import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  SET_LOADING,
  CLEAR_ERRORS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGOUT,
} from '../types';

// Load user
export const loadUser = () => async dispatch => {
  if (localStorage.token) setAuthToken(localStorage.token);
  try {
    const res = await axios.get('/api/auth');
    dispatch({type: USER_LOADED, payload: res.data});
  } catch (err) {
    dispatch({type: AUTH_ERROR, payload: null});
  }
};

// Login user
export const loginUser = formData => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    setLoading();
    const res = await axios.post('/api/auth', formData, config);
    dispatch({type: LOGIN_SUCCESS, payload: res.data});
    loadUser();
  } catch (err) {
    dispatch({type: LOGIN_FAIL, payload: err.response.data});
  }
};

// Register student
export const registerStudent = formData => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    setLoading();
    const res = await axios.post('/api/students', formData, config);
    dispatch({type: REGISTER_SUCCESS, payload: res.data});
    loadUser();
  } catch (err) {
    dispatch({type: REGISTER_FAIL, payload: err.response.data});
  }
};

// Register faculty
export const registerFaculty = formData => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    setLoading();
    const res = await axios.post('/api/faculties', formData, config);
    dispatch({type: REGISTER_SUCCESS, payload: res.data});
    loadUser();
  } catch (err) {
    dispatch({type: REGISTER_FAIL, payload: err.response.data});
  }
};

export const logOut = () => async dispatch => {
  dispatch({type: LOGOUT, payload: null});
};

// Set loading to true
export const setLoading = () => {
  return {type: SET_LOADING};
};

// Clear errors
export const clearErrors = () => {
  return {type: CLEAR_ERRORS};
};
