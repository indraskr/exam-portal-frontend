import axios from 'axios';
import {ADD_BATCH, GET_BATCHES} from '../types';

export const addBatch = name => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    const res = await axios.post('/api/batches', {name}, config);
    dispatch({type: ADD_BATCH, payload: res.data});
  } catch (err) {
    console.error(err);
  }
};

export const getBatches = () => async dispatch => {
  try {
    const res = await axios.get('/api/batches');
    dispatch({type: GET_BATCHES, payload: res.data});
  } catch (err) {
    console.error(err);
  }
};
