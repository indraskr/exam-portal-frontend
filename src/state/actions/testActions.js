import {
  ADD_TEST,
  CONFIRM_TEST,
  SET_TEMP_TEST,
  GET_TESTS,
  DISMISS_TEST,
  DELETE_TEST,
  UPDATE_TEST,
} from '../types';
import axios from 'axios';

export const addTest = test => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const {testDetails, questions, batch_id, faculty_id} = test;

  const generateQuestions = () =>
    questions.map((qsn, index) => ({
      qsn_no: index + 1,
      question: qsn.question,
      options: qsn.options,
    }));

  const generateAnswers = () =>
    questions.map((qsn, index) => ({
      qsn_no: index + 1,
      ans: Number(qsn.answer),
    }));

  const newTest = {
    faculty_id,
    batch_id,
    test_details: {
      ...testDetails,
      marks: Number(testDetails.marks),
      testTimeHours: Number(testDetails.testTimeHours),
      testTimeMinutes: Number(testDetails.testTimeMinutes),
      testDate: new Date(testDetails.testDate).getTime(),
      testStartTime: new Date(
        `${testDetails.testDate} ${testDetails.testStartTime}`
      ).getTime(),
      testEndTime: new Date(
        `${testDetails.testDate} ${testDetails.testEndTime}`
      ).getTime(),
    },
    questions: generateQuestions(),
    answers: generateAnswers(),
  };
  try {
    await axios.post('/api/tests', newTest, config);
    dispatch({type: ADD_TEST});
  } catch (err) {
    console.error(err);
  }
};

export const updateTest = ({test, oldAns, testID}) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const {testDetails, questions, batch_id, faculty_id} = test;

  const generateQuestions = () =>
    questions.map((qsn, index) =>
      !qsn.hasOwnProperty('_id')
        ? {
            qsn_no: index + 1,
            question: qsn.question,
            options: qsn.options,
          }
        : qsn
    );
  const generateAnswers = () =>
    questions.map((qsn, index) => {
      if (qsn.hasOwnProperty('_id')) {
        const ans = oldAns.find(ans => ans.qsn_no === qsn.qsn_no).ans;
        return {
          qsn_no: qsn.qsn_no,
          ans,
        };
      } else {
        return {
          qsn_no: index + 1,
          ans: Number(qsn.answer),
        };
      }
    });

  const newTest = {
    faculty_id,
    batch_id,
    test_details: {
      ...testDetails,
      marks: Number(testDetails.marks),
      testTimeHours: Number(testDetails.testTimeHours),
      testTimeMinutes: Number(testDetails.testTimeMinutes),
      testDate: new Date(testDetails.testDate).getTime(),
      testStartTime: new Date(
        `${testDetails.testDate} ${testDetails.testStartTime}`
      ).getTime(),
      testEndTime: new Date(
        `${testDetails.testDate} ${testDetails.testEndTime}`
      ).getTime(),
    },
    questions: generateQuestions(),
    answers: generateAnswers(),
  };

  console.log(newTest);

  try {
    await axios.put(`/api/tests/${testID}`, newTest, config);
    dispatch({type: UPDATE_TEST});
  } catch (err) {
    console.error(err);
  }
};

export const getAllTest = batch_id => async dispatch => {
  try {
    const res = await axios.get('/api/tests', {params: {batch_id}});
    dispatch({type: GET_TESTS, payload: res.data});
  } catch (err) {
    console.error(err);
  }
};

export const setTempTest = testID => async dispatch => {
  // TODO: finish update test
  dispatch({type: SET_TEMP_TEST, payload: testID});
};

export const confirmTest = ({testID, faculty_id}) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    await axios.post(`/api/tests/${testID}`, {faculty_id}, config);
    dispatch({type: CONFIRM_TEST, payload: testID});
  } catch (err) {
    console.error(err);
  }
};

export const dismissTest = ({testID, faculty_id}) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    await axios.post(`/api/tests/dismiss/${testID}`, {faculty_id}, config);
    dispatch({type: DISMISS_TEST, payload: testID});
  } catch (err) {
    console.error(err);
  }
};

export const deleteTest = ({testID}) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    await axios.delete(`/api/tests/${testID}`, config);
    dispatch({type: DELETE_TEST, payload: testID});
  } catch (err) {
    console.error(err);
  }
};
