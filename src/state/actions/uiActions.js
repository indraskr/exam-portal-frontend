import {CHANGE_MAIN_CONTENT} from '../types';

export const ChangeMainContent = componentName => dispatch => {
  dispatch({type: CHANGE_MAIN_CONTENT, payload: componentName});
};
