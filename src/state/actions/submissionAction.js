import axios from 'axios';
import {
  ADD_SUBMISSION,
  EVALUATE_SUBMISSION,
  GET_SUBMISSIONS,
  SUBMISSION_ERROR,
} from '../types';

export const addSubmission = submissionData => async dispatch => {
  submissionData.submitted_ans.map(
    item => (item.ans = Number(item.ans[item.ans.length - 1]))
  );
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    await axios.post('/api/submissions', submissionData, config);
    dispatch({type: ADD_SUBMISSION});
  } catch (err) {
    dispatch({type: SUBMISSION_ERROR, payload: err.response.data});
  }
};

export const getSubmissions = () => async dispatch => {
  try {
    const res = await axios.get('/api/submissions');
    dispatch({type: GET_SUBMISSIONS, payload: res.data});
  } catch (err) {
    console.error(err);
  }
};

export const evaluateSubmission = (
  faculty_id,
  submission_id
) => async dispatch => {
  try {
    await axios.post(`/api/submissions/${submission_id}`, {faculty_id});
    dispatch({type: EVALUATE_SUBMISSION, payload: submission_id});
  } catch (err) {
    console.error(err);
  }
};
