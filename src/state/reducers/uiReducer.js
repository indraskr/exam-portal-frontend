import {CHANGE_MAIN_CONTENT} from '../types';

const initialState = {
  mainContent: 'Tests',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_MAIN_CONTENT:
      return {...state, mainContent: action.payload};

    default:
      return state;
  }
};
