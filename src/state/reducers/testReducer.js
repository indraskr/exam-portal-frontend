import {
  ADD_TEST,
  UPDATE_TEST,
  CONFIRM_TEST,
  SET_TEMP_TEST,
  GET_TESTS,
  DISMISS_TEST,
  DELETE_TEST,
} from '../types';

const initialState = {
  tests: [],
  tempTest: null,
  loading: true,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TEST:
    case UPDATE_TEST:
      return {
        ...state,
        loading: false,
      };

    case GET_TESTS:
      return {
        ...state,
        tests: action.payload,
        loading: false,
      };

    case SET_TEMP_TEST:
      return {
        ...state,
        tempTest: state.tests.find(test => test._id === action.payload),
        loading: false,
      };

    case CONFIRM_TEST:
      return {
        ...state,
        tests: state.tests.map(test =>
          test._id === action.payload ? {...test, isConfirmed: true} : test
        ),
        loading: false,
      };

    case DISMISS_TEST:
      return {
        ...state,
        tests: state.tests.map(test =>
          test._id === action.payload ? {...test, isCompleted: true} : test
        ),
        loading: false,
      };

    case DELETE_TEST:
      return {
        ...state,
        tests: state.tests.map(test => test._id !== action.payload && test),
        loading: false,
      };

    default:
      return state;
  }
};
