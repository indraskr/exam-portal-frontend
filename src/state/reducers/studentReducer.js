import {GET_STUDENTS} from '../types';

const initialState = {
  students: [],
  loading: true,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;

    case GET_STUDENTS:
      return {
        ...state,
        students: action.payload,
        loading: false,
      };
  }
};
