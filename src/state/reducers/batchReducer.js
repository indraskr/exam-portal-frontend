import {ADD_BATCH, GET_BATCHES} from '../types';

const initialState = {
  batches: [],
  loading: true,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_BATCH:
      return {
        ...state,
        batches: [...state.batches, action.payload],
        loading: false,
      };

    case GET_BATCHES:
      return {
        ...state,
        batches: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
