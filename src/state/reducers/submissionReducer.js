import {
  ADD_SUBMISSION,
  CLEAR_ERRORS,
  EVALUATE_SUBMISSION,
  GET_SUBMISSIONS,
  SUBMISSION_ERROR,
} from '../types';

const initialState = {
  submissions: null,
  loading: true,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_SUBMISSION:
      return {...state, loading: false};

    case GET_SUBMISSIONS:
      return {...state, submissions: action.payload, loading: false};

    case SUBMISSION_ERROR:
      return {...state, error: action.payload, loading: false};

    case EVALUATE_SUBMISSION:
      return {
        ...state,
        submissions: state.submissions.map(sub =>
          sub._id === action.payload ? {...sub, isEvaluated: true} : sub
        ),
        loading: false,
      };

    case CLEAR_ERRORS:
      return {...state, error: null};

    default:
      return state;
  }
};
