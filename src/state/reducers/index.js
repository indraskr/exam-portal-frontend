import {combineReducers} from 'redux';
import authReducer from './authReducer';
import uiReducer from './uiReducer';
import testReducer from './testReducer';
import batchReducer from './batchReducer';
import studentReducer from './studentReducer';
import submissionReducer from './submissionReducer';

export default combineReducers({
  auth: authReducer,
  ui: uiReducer,
  test: testReducer,
  batch: batchReducer,
  student: studentReducer,
  submission: submissionReducer,
});
