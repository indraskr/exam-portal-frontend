import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';
import isBetween from 'dayjs/plugin/isBetween';

export const isTestDatePassed = ({testDate}) => {
  dayjs.extend(isToday);
  const _isBefore = dayjs(testDate).isBefore(dayjs());
  const _isToday = dayjs(testDate).isToday();
  return _isBefore === true && _isToday === true ? false : _isBefore;
};

export const isTestAvailable = ({testStartTime, testEndTime}) => {
  dayjs.extend(isBetween);
  return dayjs().isBetween(dayjs(testStartTime), dayjs(testEndTime));
};

export const isTestEnded = testEndTime => {
  return dayjs().isAfter(dayjs(testEndTime));
};
