import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';
import {loadUser} from '../../state/actions/authActions';

const FacultyRoute = ({
  component: Component,
  loadUser,
  isAuthenticated,
  loading,
  user,
  ...rest
}) => {
  useEffect(() => {
    loadUser();
    // eslint-disable-next-line
  }, []);

  return (
    <Route
      {...rest}
      render={props => {
        const canAccess = user !== null && isAuthenticated && !loading;
        if (canAccess && user.isFaculty) return <Component {...props} />;
        if (canAccess && user.isStudent) return <Redirect to='/' />;
        if (!isAuthenticated && !loading) return <Redirect to='/login' />;
      }}
    />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  loading: state.auth.loading,
  user: state.auth.user,
});

export default connect(mapStateToProps, {loadUser})(FacultyRoute);
