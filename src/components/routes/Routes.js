import React, {Suspense} from 'react';
import {Route, Switch} from 'react-router-dom';
import ErrorPage from '../pages/ErrorPage';
import Faculty from '../pages/faculty';
import Student from '../pages/student';
import TestTaker from '../pages/TestTaker';
import AddTest from '../tests/AddTest';
import FacultyRoute from './FacultyRoute';
import PrivateRoute from './PrivateRoute';

const Register = React.lazy(() => import('../auth/register'));
const FacultyRegister = React.lazy(() => import('../auth/FacultyRegister'));
const Login = React.lazy(() => import('../auth/login'));

const Routes = () => {
  const LazyRoute = ({component: Component, ...rest}) => (
    <Route
      {...rest}
      render={props => (
        <Suspense fallback={<div>Loading content...</div>}>
          <Component {...props} />
        </Suspense>
      )}
    />
  );

  return (
    <Switch>
      <PrivateRoute exact path='/' component={Student} />
      <Route exact path='/test/:id' component={TestTaker} />

      <LazyRoute exact path='/register' component={Register} />
      <LazyRoute exact path='/faculty/register' component={FacultyRegister} />
      <LazyRoute exact path='/login' component={Login} />

      <FacultyRoute exact path='/faculty' component={Faculty} />
      <FacultyRoute exact path='/faculty/addTest' component={AddTest} />
      <FacultyRoute exact path='/faculty/test/:id' component={AddTest} />
      <Route path='*'>
        <ErrorPage />
      </Route>
    </Switch>
  );
};

export default Routes;
