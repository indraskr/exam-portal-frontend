import {Badge, Box, Button, Flex, Heading} from '@chakra-ui/core';
import React from 'react';
import {Link} from 'react-router-dom';

const StudentTestItem = ({test, testTimeStatus, isTestDatePassed}) => {
  const {isConfirmed, isCompleted, test_details} = test;

  const TestStatusBadge = props => {
    let status = {
      text: '',
      color: '',
    };
    if (isConfirmed && !isCompleted && !isTestDatePassed && testTimeStatus)
      status = {text: 'ongoing', color: 'green'};
    if (!isCompleted && !isTestDatePassed && !testTimeStatus)
      status = {text: 'upcoming', color: 'yellow'};

    return (
      <Badge mx={2} variant='subtle' variantColor={status.color} {...props}>
        {status.text}
      </Badge>
    );
  };

  return (
    <Flex
      bg='gray.200'
      borderRadius={4}
      p={4}
      mb={2}
      shadow='md'
      borderWidth='1px'>
      <Box mr='auto'>
        <Heading
          isTruncated
          w={{base: '16rem', md: '22rem', lg: '25rem'}}
          fontSize='xl'>
          {test_details.name}
          <TestStatusBadge />
        </Heading>
        <p>{`${test_details.subject} - ${test_details.marks} marks`}</p>
      </Box>
      <Link to={`/test/${test._id}`}>
        {testTimeStatus && (
          <Button variantColor='blue' variant='outline'>
            Start
          </Button>
        )}
      </Link>
    </Flex>
  );
};

export default StudentTestItem;
