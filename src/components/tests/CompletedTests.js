import {Stack} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import {isTestAvailable, isTestEnded} from '../../utils/helpers';
import CompletedTestItem from './CompletedTestItem';

const CompletedTests = ({tests, user}) => {
  const isStudentSubmittedTest = submitted_by =>
    user.student !== undefined
      ? submitted_by.includes(user.student._id)
      : false;

  const RenderTests = () => {
    if (user.isStudent) {
      return tests.map(
        test =>
          test.isConfirmed &&
          isStudentSubmittedTest(test.submitted_by) && (
            <CompletedTestItem
              key={test._id}
              test={test}
              isStudent={user.isStudent}
            />
          )
      );
    }

    if (user.isFaculty) {
      return tests.map(
        test =>
          // isTestDatePassed(test.test_details) &&
          isTestEnded(test.test_details.testEndTime) &&
          test.isConfirmed &&
          !isTestAvailable(test.test_details) && (
            <CompletedTestItem
              key={test._id}
              test={test}
              isStudent={user.isStudent}
            />
          )
      );
    }
  };

  return (
    <>
      <Stack spacing={3} w='full'>
        {<RenderTests />}
      </Stack>
    </>
  );
};

const mapStateToProps = state => ({
  tests: state.test.tests,
  user: state.auth.user,
});

export default connect(mapStateToProps, {})(CompletedTests);
