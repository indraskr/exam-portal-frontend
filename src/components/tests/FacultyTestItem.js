import {
  Badge,
  Box,
  Flex,
  Heading,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  Tooltip,
} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {
  confirmTest,
  deleteTest,
  dismissTest,
} from '../../state/actions/testActions';
import PopOverButton from '../layouts/PopOverButton';

const FacultyTestItem = ({
  test,
  confirmTest,
  user,
  batches,
  isTestDatePassed,
  deleteTest,
  testTimeStatus,
}) => {
  const {_id: testID, isConfirmed, isCompleted} = test;

  const onConfirmBtnClick = () => {
    confirmTest({testID, faculty_id: user.faculty._id});
  };

  const getBatchName = () => {
    const batch = batches.find(batch => batch._id === test.batch_id);
    return batch ? batch.name.toUpperCase() : null;
  };

  const TestStatusBadge = props => {
    let status = {
      text: '',
      color: '',
    };
    if (isConfirmed && !isCompleted && !isTestDatePassed && testTimeStatus)
      status = {text: 'ongoing', color: 'green'};
    if (!isCompleted && !isTestDatePassed && !testTimeStatus)
      status = {text: 'upcoming', color: 'yellow'};

    return (
      <Badge mx={2} variant='subtle' variantColor={status.color} {...props}>
        {status.text}
      </Badge>
    );
  };

  return (
    <Flex
      bg='gray.200'
      borderRadius={4}
      p={4}
      mb={2}
      shadow='md'
      borderWidth='1px'>
      <Box mr='auto'>
        <Heading
          isTruncated
          w={{base: '16rem', md: '22rem', lg: '25rem'}}
          fontSize='xl'>
          {test.test_details.name}
          <TestStatusBadge />
        </Heading>
        <Text>{getBatchName()}</Text>
      </Box>

      <Box display={{base: 'none', md: 'block'}}>
        {!isConfirmed && (
          <>
            <PopOverButton
              headerMsg='Confirm this test?'
              bodyMsg='After confirming the test, students can see the test on their list.'
              onClick={onConfirmBtnClick}>
              <IconButton
                title='Confirm test'
                variant='outline'
                variantColor='blue'
                aria-label='Confirm test'
                icon='check'
                mr='0.5rem'
              />
            </PopOverButton>
            <Tooltip hasArrow label='Edit this test' placement='bottom'>
              <Link to={`/faculty/test/${testID}?edit=true`}>
                <IconButton
                  variant='outline'
                  variantColor='blue'
                  aria-label='Edit test'
                  icon='edit'
                  mr='0.5rem'
                />{' '}
              </Link>
            </Tooltip>
            <Tooltip hasArrow label='Delete this test' placement='bottom'>
              <IconButton
                onClick={() => deleteTest({testID})}
                variant='outline'
                variantColor='blue'
                aria-label='Delete test'
                icon='delete'
                mr='0.5rem'
              />
            </Tooltip>
          </>
        )}

        {/* <Link to={`/faculty/test/${testID}`}>
            <Button variantColor='blue' variant='outline'>
              View
            </Button>
          </Link> */}
      </Box>

      <Box display={{base: 'block', md: 'none'}}>
        <Menu>
          <MenuButton
            as={IconButton}
            variant='outline'
            variantColor='blue'
            aria-label='Open menu'
            icon='chevron-down'
            fontSize='1.8rem'
          />
          <MenuList>
            {/* <MenuItem as='a' href='#'>
              View
            </MenuItem> */}
            <MenuItem as='a' href='#'>
              Edit test
            </MenuItem>
            <MenuItem as='a' href='#'>
              Confirm test
            </MenuItem>
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
  batches: state.batch.batches,
});

export default connect(mapStateToProps, {confirmTest, dismissTest, deleteTest})(
  FacultyTestItem
);
