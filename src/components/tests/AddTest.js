import {
  Box,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Radio,
  RadioGroup,
  Stack,
  Text,
  useDisclosure,
} from '@chakra-ui/core';
import React, {useState} from 'react';
import {FaRegSave} from 'react-icons/fa';
import {connect} from 'react-redux';
import {useHistory, useLocation, useParams} from 'react-router-dom';
import {
  addTest,
  setTempTest,
  updateTest,
  getAllTest,
} from '../../state/actions/testActions';
import CustomIconButton from '../layouts/CustomIconButton';
import AddQuestionModal from './AddQuestionModal';
import TestDetailsForm from './TestDetailsForm';
import {useEffect} from 'react';

const AddTest = ({
  addTest,
  updateTest,
  setTempTest,
  getAllTest,
  tempTest,
  batches,
  user,
}) => {
  const history = useHistory();
  const {onOpen, isOpen, onClose} = useDisclosure();

  const [test, setTest] = useState({
    testDetails: null,
    questions: [],
  });

  // get query params for test id
  const useQueryParams = queryString => {
    let query = new URLSearchParams(useLocation().search);
    return query.get(queryString);
  };
  const {id} = useParams();
  const editMode = useQueryParams('edit');

  // sets temp test to test state, for update test
  useEffect(() => {
    editMode && setTempTest(id);
    if (tempTest !== null && editMode) {
      setTest({...test, questions: tempTest.questions});
    }
    // eslint-disable-next-line
  }, [tempTest]);

  // handles questions
  const addQuestion = (question, options) => {
    setTest({
      ...test,
      questions: [...test.questions, {...question, options: {...options}}],
    });
  };

  // handles test details inputs
  const addTestDetails = testDetails => {
    const batch_id = batches.find(batch => batch.name === testDetails.batch_id)
      ._id;
    const faculty_id = user.faculty._id;
    setTest({...test, batch_id, faculty_id, testDetails: {...testDetails}});
  };

  const onAddTestBtnClick = () => {
    if (!editMode) {
      addTest(test);
      getAllTest();
    } else {
      updateTest({test, oldAns: tempTest.answers, testID: id});
    }
    history.push('/faculty');
  };

  // handles rendering questions as they change
  const renderQuestions = () => {
    if (test.questions.length > 0) {
      return test.questions.map((qsn, index) => (
        <Box key={index} bg='gray.300' p={5} rounded='md'>
          <FormControl as='fieldset'>
            <FormLabel as='legend'>{qsn.question}</FormLabel>
            <RadioGroup>
              {Object.values(qsn.options).map((option, idx) => (
                <Radio key={idx} value={option}>
                  {option}
                </Radio>
              ))}
            </RadioGroup>
          </FormControl>
        </Box>
      ));
    } else {
      return (
        <Text
          textAlign='center'
          fontSize='xl'
          color='gray.400'
          top='50%'
          left='50%'
          transform='translate(-50%,40%)'
          pos='absolute'>
          No questions here, add new questions
        </Text>
      );
    }
  };

  return (
    <Box w='full' bg='gray.300' p={5}>
      <CustomIconButton
        onClick={onAddTestBtnClick}
        pos='fixed'
        right='1rem'
        top='1rem'
        leftIcon={FaRegSave}>
        {editMode ? 'Update test' : 'Add test'}
      </CustomIconButton>
      <Flex
        w={{sm: '100%', md: '80%', lg: '60%'}}
        m='0 auto'
        justify='center'
        align='center'
        flexDir='column'>
        {/* TEST DETAILS SECTION */}
        <TestDetailsForm
          addTestDetails={addTestDetails}
          test_details={
            tempTest !== null && editMode ? tempTest.test_details : null
          }
        />

        {/* QUESTIONS SECTION */}
        <Box pos='relative' w='100%' m='1rem 0' bg='gray.200' p={10}>
          <Heading as='h2' size='lg' mb={4}>
            Questions
          </Heading>
          <Stack minH='30vh' spacing={5}>
            {renderQuestions()}
          </Stack>

          {/* ADD NEW QUESTION BUTTON */}
          <CustomIconButton
            // isDisabled={editMode}
            onClick={onOpen}
            pos='absolute'
            right='0'
            transform='translate(-10%,50%)'
            leftIcon='add'>
            Add question
          </CustomIconButton>
          <AddQuestionModal
            addQuestion={addQuestion}
            isOpen={isOpen}
            onClose={onClose}
          />
        </Box>
      </Flex>
    </Box>
  );
};

const mapStateToProps = state => ({
  tempTest: state.test.tempTest,
  batches: state.batch.batches,
  user: state.auth.user,
});

export default connect(mapStateToProps, {
  addTest,
  updateTest,
  getAllTest,
  setTempTest,
})(AddTest);
