import {
  Accordion,
  AccordionHeader,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Flex,
  Heading,
  Icon,
  List,
  ListIcon,
  ListItem,
  Text,
} from '@chakra-ui/core';
import React from 'react';

const TestDetailsAccordion = ({testDetails, timeLeft}) => {
  return (
    <Accordion allowToggle allowMultiple my={3}>
      <AccordionItem border='1px solid' borderColor='gray.500' rounded='lg'>
        <AccordionHeader title='Click to show test details' py={3}>
          <Flex
            justify='space-between'
            px={{base: 0, sm: 2}}
            flex='1'
            textAlign='left'>
            <Heading size='lg'>{testDetails.name}</Heading>
            <Flex align='center'>
              <Icon mr={1} name='time' />
              <Text>{timeLeft}m left</Text>
            </Flex>
          </Flex>
          <AccordionIcon />
        </AccordionHeader>
        <AccordionPanel pb={4}>
          <List px={2} spacing={3}>
            <ListItem>
              <ListIcon icon='arrow-forward' color='gray.600' />
              Subject: {testDetails.subject}
            </ListItem>
            <ListItem>
              <ListIcon icon='arrow-forward' color='gray.600' />
              Marks: {testDetails.marks}
            </ListItem>
          </List>
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
};

export default TestDetailsAccordion;
