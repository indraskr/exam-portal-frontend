import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
} from '@chakra-ui/core';
import React, {useState} from 'react';

const AddQuestionModal = ({isOpen, onClose, addQuestion}) => {
  const [question, setQuestion] = useState({
    question: '',
    answer: '',
  });

  const onChange = e => {
    setQuestion({...question, [e.target.name]: e.target.value});
  };

  const [options, setOptions] = useState({
    option1: '',
    option2: '',
    option3: '',
    option4: '',
  });

  const onOptionChange = e => {
    setOptions({
      ...options,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    addQuestion(question, options);
    onClose();
  };

  return (
    <Modal
      size='xl'
      closeOnOverlayClick={false}
      isOpen={isOpen}
      onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Add a new question</ModalHeader>
        <ModalCloseButton />
        <form onSubmit={onSubmit}>
          <ModalBody>
            <FormControl isRequired>
              <FormLabel htmlFor='question'>Question</FormLabel>
              <Input
                placeholder='Add question'
                type='text'
                id='question'
                name='question'
                onChange={onChange}
              />
            </FormControl>
            <Stack isInline pt={3}>
              <FormControl flex='1' isRequired>
                <FormLabel htmlFor='option1'>Option 1</FormLabel>
                <Input
                  type='text'
                  id='option1'
                  name='option1'
                  onChange={onOptionChange}
                />
              </FormControl>
              <FormControl flex='1' isRequired>
                <FormLabel htmlFor='option2'>Option 2</FormLabel>
                <Input
                  type='text'
                  id='option2'
                  name='option2'
                  onChange={onOptionChange}
                />
              </FormControl>
            </Stack>
            <Stack isInline pt={3}>
              <FormControl flex='1' isRequired>
                <FormLabel htmlFor='option3'>Option 3</FormLabel>
                <Input
                  type='text'
                  id='option3'
                  name='option3'
                  onChange={onOptionChange}
                />
              </FormControl>
              <FormControl flex='1' isRequired>
                <FormLabel htmlFor='option4'>Option 4</FormLabel>
                <Input
                  type='text'
                  id='option4'
                  name='option4'
                  onChange={onOptionChange}
                />
              </FormControl>
            </Stack>
            <FormControl isRequired pt={3}>
              <FormLabel htmlFor='answer'>Answer</FormLabel>
              <Input
                type='number'
                max='4'
                min='1'
                id='answer'
                name='answer'
                placeholder='Add number between 1-4'
                onChange={onChange}
              />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button type='submit' variantColor='blue' mr={3}>
              Add
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default AddQuestionModal;
