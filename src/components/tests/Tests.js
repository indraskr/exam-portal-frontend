import {Stack} from '@chakra-ui/core';
import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import FAB from '../layouts/FAB';
import FacultyTestItem from './FacultyTestItem';
import {connect} from 'react-redux';
import StudentTestItem from './StudentTestItem';
import {
  isTestAvailable,
  isTestDatePassed,
  isTestEnded,
} from '../../utils/helpers';

const Tests = ({tests, user}) => {
  const isTestAlreadySubmitted = submitted_by =>
    submitted_by.includes(user.student._id) ? true : false;

  return (
    <Fragment>
      <Stack spacing={3} w='full'>
        {user.isStudent &&
          tests.map(test => {
            const condition =
              !isTestAlreadySubmitted(test.submitted_by) &&
              // isTestAvailable(test.test_details) &&
              !isTestEnded(test.test_details.testEndTime) &&
              !isTestDatePassed(test.test_details) &&
              user.student.batch_id === test.batch_id &&
              test.isConfirmed &&
              !test.isCompleted;
            return (
              condition && (
                <StudentTestItem
                  key={test._id}
                  test={test}
                  testTimeStatus={isTestAvailable(test.test_details)}
                  isTestDatePassed={isTestDatePassed(test.test_details)}
                />
              )
            );
          })}

        {user.isFaculty &&
          tests.map(
            test =>
              user.faculty._id === test.faculty_id &&
              // isTestAvailable(test.test_details) &&
              !isTestEnded(test.test_details.testEndTime) &&
              !isTestDatePassed(test.test_details) && (
                <FacultyTestItem
                  key={test._id}
                  test={test}
                  testTimeStatus={isTestAvailable(test.test_details)}
                  isTestDatePassed={isTestDatePassed(test.test_details)}
                />
              )
          )}
      </Stack>

      {user.isFaculty && (
        <Link to='/faculty/addTest'>
          <FAB tooltipLabel='Add new test' />
        </Link>
      )}
    </Fragment>
  );
};

const mapStateToProps = state => ({
  tests: state.test.tests,
  user: state.auth.user,
});

export default connect(mapStateToProps, {})(Tests);
