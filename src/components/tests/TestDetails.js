import React from 'react';
import {useParams} from 'react-router-dom';

const TestDetails = () => {
  const {id} = useParams();
  return <div>TestDetails {id}</div>;
};

export default TestDetails;
