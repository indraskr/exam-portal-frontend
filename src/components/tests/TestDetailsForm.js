import {
  Box,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Select,
  Stack,
} from '@chakra-ui/core';
import dayjs from 'dayjs';
import React, {useState, useEffect} from 'react';
import BatchListOptions from '../batch/BatchListOptions';
import CustomIconButton from '../layouts/CustomIconButton';

const TestDetailsForm = ({addTestDetails, test_details}) => {
  const today = dayjs().format('YYYY-MM-DD');
  const [testDetails, setTestDetails] = useState({
    name: '',
    marks: 0,
    subject: '',
    testTimeHours: 0,
    testTimeMinutes: 0,
    testDate: today,
    testStartTime: '10:00',
    testEndTime: '23:59',
  });

  useEffect(() => {
    // sets test details in state, only when updating test
    if (test_details !== null) {
      const {
        name,
        marks,
        subject,
        testTimeHours,
        testTimeMinutes,
        testDate,
        testStartTime,
        testEndTime,
      } = test_details;

      setTestDetails({
        ...testDetails,
        name,
        marks,
        subject,
        testTimeHours,
        testTimeMinutes,
        testDate: dayjs(testDate).format('YYYY-MM-DD'),
        testStartTime: dayjs(testStartTime).format('HH:mm'),
        testEndTime: dayjs(testEndTime).format('HH:mm'),
      });
    }
    // eslint-disable-next-line
  }, [test_details]);

  const {
    name,
    subject,
    testDate,
    testStartTime,
    testEndTime,
    marks,
    testTimeHours,
    testTimeMinutes,
  } = testDetails;

  const handleChange = e =>
    setTestDetails({...testDetails, [e.target.name]: e.target.value});

  const onSubmit = e => {
    e.preventDefault();
    addTestDetails(testDetails);
  };

  return (
    <Box pos='relative' w='100%' m='1rem 0' bg='gray.200' p={10} shadow='md'>
      <form onSubmit={onSubmit}>
        <Heading as='h2' size='lg' mb={4}>
          Test details
        </Heading>
        <Stack spacing={3}>
          <FormControl isRequired>
            <FormLabel htmlFor='testName'>Test name</FormLabel>
            <Input
              onChange={handleChange}
              value={name}
              id='testName'
              name='name'
              type='text'
              placeholder='Test name'
            />
          </FormControl>

          <Stack spacing={2} isInline>
            <FormControl isRequired flex='5'>
              <FormLabel htmlFor='subName'>Subject name</FormLabel>
              <Input
                onChange={handleChange}
                value={subject}
                id='subName'
                name='subject'
                type='text'
                placeholder='Subject name'
              />
            </FormControl>
            <FormControl isRequired flex='3'>
              <FormLabel htmlFor='marks'>Full marks</FormLabel>
              <Input
                onChange={handleChange}
                value={marks}
                id='marks'
                name='marks'
                type='number'
                min='5'
                placeholder='Full marks'
              />
            </FormControl>
          </Stack>

          <Stack isInline spacing={2}>
            <FormControl isRequired flex='2'>
              <FormLabel htmlFor='batch_id'>Batch</FormLabel>
              <Select
                onChange={handleChange}
                // value=''
                icon='triangle-down'
                iconSize={3}
                id='batch_id'
                name='batch_id'
                placeholder='Select batch'>
                <BatchListOptions />
              </Select>
            </FormControl>
            <FormControl isRequired flex='1'>
              <FormLabel htmlFor='hour'>Hours</FormLabel>
              <Input
                onChange={handleChange}
                value={testTimeHours}
                id='hour'
                name='testTimeHours'
                type='number'
                placeholder='Add hours'
                min='0'
                max='4'
              />
            </FormControl>
            <FormControl isRequired flex='1'>
              <FormLabel htmlFor='minute'>Minutes</FormLabel>
              <Input
                onChange={handleChange}
                value={testTimeMinutes}
                id='minute'
                name='testTimeMinutes'
                type='number'
                placeholder='Add minutes'
                min='0'
                max='60'
              />
            </FormControl>
          </Stack>

          <Stack isInline spacing={2}>
            <FormControl isRequired flex='1'>
              <FormLabel htmlFor='startTime'>Test start time</FormLabel>
              <Input
                onChange={handleChange}
                value={testStartTime}
                id='startTime'
                name='testStartTime'
                type='time'
              />
            </FormControl>
            <FormControl isRequired flex='1'>
              <FormLabel htmlFor='endTime'>Test end time</FormLabel>
              <Input
                onChange={handleChange}
                value={testEndTime}
                id='endTime'
                name='testEndTime'
                type='time'
              />
            </FormControl>
            <FormControl isRequired flex='1'>
              <FormLabel htmlFor='testDate'>Test date</FormLabel>
              <Input
                onChange={handleChange}
                id='testDate'
                name='testDate'
                type='date'
                value={testDate}
                min={today}
              />
            </FormControl>
          </Stack>
        </Stack>

        <CustomIconButton
          loadingText='Saving'
          type='submit'
          pos='absolute'
          right='0'
          transform='translate(-10%,50%)'
          leftIcon='check'>
          Save details
        </CustomIconButton>
      </form>
    </Box>
  );
};

export default TestDetailsForm;
