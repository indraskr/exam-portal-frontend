import {Box, Button, Flex, Heading, IconButton} from '@chakra-ui/core';
import dayjs from 'dayjs';
import React from 'react';
import {connect} from 'react-redux';
import {dismissTest} from '../../state/actions/testActions';
import PopOverButton from '../layouts/PopOverButton';

const CompletedTestItem = ({test, isStudent, user, dismissTest}) => {
  const {test_details, isCompleted, _id: testID} = test;
  return (
    <Flex
      bg='gray.200'
      borderRadius={4}
      p={4}
      mb={2}
      shadow='md'
      borderWidth='1px'
      border={!isCompleted && !isStudent && '1px solid #F56565'}>
      <Box mr='auto'>
        <Heading
          isTruncated
          w={{base: '16rem', md: '22rem', lg: '25rem'}}
          fontSize='xl'>
          {test_details.name}
        </Heading>
        <p>{dayjs(test_details.testDate).format('MMM DD, YYYY')}</p>
      </Box>
      {/* TODO: show test details in modal */}
      {isStudent && (
        <Button variantColor='blue' isDisabled={!isCompleted} variant='outline'>
          Check Result
        </Button>
      )}
      {!isCompleted && !isStudent && (
        <PopOverButton
          headerMsg='Mark test as completed'
          bodyMsg='Students can not view their result until the test marked as completed'
          onClick={() => dismissTest({testID, faculty_id: user.faculty._id})}>
          <IconButton
            title='Mark as completed'
            variant='outline'
            variantColor='blue'
            aria-label='Dismiss test'
            icon='check-circle'
            mr='0.5rem'
          />
        </PopOverButton>
      )}
    </Flex>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, {dismissTest})(CompletedTestItem);
