import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import {evaluateSubmission} from '../../state/actions/submissionAction';

const SubmissionDetailsModal = ({
  onClose,
  isOpen,
  submitted_ans,
  test_id,
  tests,
  faculty_id,
  submission_id,
  evaluateSubmission,
}) => {
  const checkAnswers = () => {
    let SubmittedAnsObject = {};
    submitted_ans.forEach(s => {
      SubmittedAnsObject[s.qsn_no] = s.ans;
    });
    // TODO: error control
    const actualAnswers = tests.find(test => test._id === test_id).answers;

    let correct = 0;
    actualAnswers.forEach(
      a => SubmittedAnsObject[a.qsn_no] === a.ans && correct++
    );

    return {correct, length: actualAnswers.length};
  };

  const {correct, length} = checkAnswers();
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Modal Title</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          Got {correct} right out of {length}
        </ModalBody>
        <ModalFooter>
          <Button
            variant='solid'
            onClick={() => evaluateSubmission(faculty_id, submission_id)}>
            Evaluate
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

const mapStateToProps = state => ({
  tests: state.test.tests,
});

export default connect(mapStateToProps, {evaluateSubmission})(
  SubmissionDetailsModal
);
