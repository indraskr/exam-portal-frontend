import {
  Badge,
  Box,
  Button,
  Flex,
  Heading,
  Text,
  useDisclosure,
} from '@chakra-ui/core';
import React from 'react';
import SubmissionDetailsModal from './SubmissionDetailsModal';

const SubmissionItem = ({testName, studentName, batchName, submission}) => {
  const {isEvaluated, submitted_ans, test_id, faculty_id, _id} = submission;
  const {isOpen, onOpen, onClose} = useDisclosure();
  return (
    <Flex
      bg='gray.200'
      borderRadius={4}
      p={4}
      mb={2}
      shadow='md'
      borderWidth='1px'>
      <Box mr='auto'>
        <Heading
          isTruncated
          w={{base: '16rem', md: '22rem', lg: '25rem'}}
          fontSize='xl'>
          {testName}
          {!isEvaluated && (
            <Badge mb={1} ml={2} variant='subtle' variantColor='green'>
              new
            </Badge>
          )}
        </Heading>
        <Text color='gray.600'>
          by <strong>{studentName}</strong> from <strong>{batchName}</strong>
        </Text>
      </Box>
      <Button onClick={onOpen} variantColor='blue' variant='outline'>
        View
      </Button>
      <SubmissionDetailsModal
        isOpen={isOpen}
        onClose={onClose}
        submitted_ans={submitted_ans}
        test_id={test_id}
        faculty_id={faculty_id}
        submission_id={_id}
      />
    </Flex>
  );
};

export default SubmissionItem;
