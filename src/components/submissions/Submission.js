import {Stack} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import SubmissionItem from './SubmissionItem';

const Submission = ({submission, tests, students, user, batches}) => {
  const {submissions, loading} = submission;

  const getTestAndBatchName = TestID => {
    const test = tests.find(test => test._id === TestID);
    const testName = test ? test.test_details.name : null;
    const batch = batches.find(batch => batch._id === test.batch_id);
    const batchName = batch ? batch.name.toUpperCase() : null;
    return {testName, batchName};
  };

  const studentName = id => {
    const studentFound = students.find(student => student._id === id);
    return studentFound ? studentFound.name : 'Unknown';
  };

  // checks if the submission for this faculty or not
  const isSubmissionForFaculty = facultyID =>
    user.faculty ? user.faculty._id === facultyID : false;

  return (
    <Stack spacing={3} w='full'>
      {!loading &&
        submissions.map(submission => {
          const {testName, batchName} = getTestAndBatchName(submission.test_id);
          return (
            isSubmissionForFaculty(submission.faculty_id) && (
              <SubmissionItem
                key={submission._id}
                submission={submission}
                testName={testName}
                batchName={batchName}
                studentName={studentName(submission.student_id)}
              />
            )
          );
        })}
    </Stack>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
  submission: state.submission,
  tests: state.test.tests,
  students: state.student.students,
  batches: state.batch.batches,
});

export default connect(mapStateToProps, {})(Submission);
