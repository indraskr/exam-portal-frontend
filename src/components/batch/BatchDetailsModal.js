import React from 'react';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  Accordion,
  AccordionItem,
  AccordionHeader,
  AccordionPanel,
  AccordionIcon,
  Box,
  List,
  ListIcon,
  ListItem,
} from '@chakra-ui/core';
import {BiUser} from 'react-icons/bi';
import {connect} from 'react-redux';

const BatchDetailsModal = ({
  isOpen,
  onClose,
  batchName,
  studentIds,
  students,
}) => {
  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Batch Details</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>
              <strong>Name:</strong> {batchName.toUpperCase()}
            </Text>
            <Text>
              <strong>Total students:</strong> {studentIds.length}
            </Text>

            <Accordion allowToggle pt={2}>
              <AccordionItem>
                <AccordionHeader p={0}>
                  <Box flex='1' textAlign='left'>
                    <strong>Student list</strong>
                  </Box>
                  <AccordionIcon />
                </AccordionHeader>
                <AccordionPanel pb={4}>
                  <List spacing={2}>
                    {studentIds.map(id => {
                      const student = students.find(
                        student => student._id === id
                      );
                      return (
                        student && (
                          <ListItem key={id}>
                            <ListIcon icon={BiUser} color='blue.500' />
                            {student.name}
                          </ListItem>
                        )
                      );
                    })}
                  </List>
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

const mapStateToProps = state => ({
  students: state.student.students,
});
export default connect(mapStateToProps, {})(BatchDetailsModal);
