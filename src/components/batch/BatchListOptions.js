import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {getBatches} from '../../state/actions/batchActions';

const BatchListOptions = ({batches, getBatches}) => {
  useEffect(() => {
    getBatches();
    // eslint-disable-next-line
  }, []);

  return (
    batches.length > 0 &&
    batches.map(batch => (
      <option key={batch._id} value={batch.name}>
        {batch.name.toUpperCase()}
      </option>
    ))
  );
};

const mapStateToProps = state => ({
  batches: state.batch.batches,
});

export default connect(mapStateToProps, {getBatches})(BatchListOptions);
