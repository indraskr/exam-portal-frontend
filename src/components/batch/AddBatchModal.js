import React, {useRef} from 'react';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  ModalFooter,
  Button,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/core';
import {addBatch} from '../../state/actions/batchActions';
import {connect} from 'react-redux';

const AddBatchModal = ({isOpen, onClose, addBatch}) => {
  const inputElem = useRef(null);
  const onClick = () => {
    if (!(inputElem.current.value.trim() === '')) {
      addBatch(inputElem.current.value.trim());
      onClose();
    }
  };
  return (
    <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Add a new batch</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <FormControl>
            <FormLabel>Batch name</FormLabel>
            <Input
              ref={inputElem}
              variant='filled'
              placeholder='Eg: BCA 2018-21'
            />
          </FormControl>
        </ModalBody>
        <ModalFooter>
          <Button onClick={onClick} variantColor='blue' mr={3}>
            Save
          </Button>
          <Button onClick={onClose}>Cancel</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default connect(null, {addBatch})(AddBatchModal);
