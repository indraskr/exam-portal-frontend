import {Flex, useDisclosure} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import FAB from '../layouts/FAB';
import AddBatchModal from './AddBatchModal';
import BatchItem from './BatchItem';

const Batch = ({batches}) => {
  const {onOpen, isOpen, onClose} = useDisclosure();
  return (
    <Flex w='full' wrap='wrap' justify={{base: 'center', md: 'start'}}>
      {batches.map(batch => (
        <BatchItem
          key={batch._id}
          batchName={batch.name}
          studentList={batch.student_list}
        />
      ))}
      <AddBatchModal isOpen={isOpen} onClose={onClose} />
      <FAB tooltipLabel='Add new batch' onClick={onOpen} />
    </Flex>
  );
};

const mapStateToProps = state => ({
  batches: state.batch.batches,
});

export default connect(mapStateToProps, {})(Batch);
