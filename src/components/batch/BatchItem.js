import {Flex, Text, useDisclosure} from '@chakra-ui/core';
import React from 'react';
import BatchDetailsModal from './BatchDetailsModal';

const BatchItem = ({batchName, studentList}) => {
  const {onOpen, isOpen, onClose} = useDisclosure();
  return (
    <>
      <BatchDetailsModal
        isOpen={isOpen}
        onClose={onClose}
        batchName={batchName}
        studentIds={studentList}
      />
      <Flex
        onClick={onOpen}
        m={5}
        cursor='pointer'
        h='10rem'
        w='11rem'
        justify='center'
        align='center'
        borderRadius='lg'
        bg='blue.600'
        shadow='lg'
        color='gray.200'>
        <Text fontSize='2xl' fontWeight='bold' textTransform='uppercase'>
          {batchName}
        </Text>
      </Flex>
    </>
  );
};

export default BatchItem;
