import PropTypes from 'prop-types';
import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {loginUser, clearErrors} from '../../state/actions/authActions';
import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Button,
  Input,
  InputGroup,
  InputRightElement,
  InputLeftElement,
  Icon,
  useToast,
} from '@chakra-ui/core';

const Login = props => {
  const {
    loginUser,
    error,
    clearErrors,
    isAuthenticated,
    isFaculty,
    isStudent,
  } = props;
  const [user, setUser] = useState({
    username: '',
    password: '',
  });
  const [show, setShow] = useState(false);

  const {username, password} = user;

  const onChange = e => setUser({...user, [e.target.name]: e.target.value});

  const toast = useToast();
  useEffect(() => {
    if (
      error !== null &&
      (error.id === 'INVALID_PASSWORD' || error.id === 'INVALID_CREDENTIALS')
    ) {
      toast({
        position: 'bottom',
        title: 'Error.',
        description: error.msg,
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
      clearErrors();
    }
    if (isAuthenticated && isFaculty) {
      props.history.push('/faculty');
    }
    if (isAuthenticated && isStudent) {
      props.history.push('/');
    }
    // eslint-disable-next-line
  }, [error, props.history, isAuthenticated, isFaculty]);

  const onSubmit = async e => {
    e.preventDefault();
    if (username === '' || password === '') {
      console.log('Please fill out all required fields');
    } else {
      loginUser({username, password});
    }
  };
  return (
    <Flex w='100%' h='100vh' align='center' justify='center' direction='column'>
      <Box
        w={{base: '90%', sm: '80%', md: '35%'}}
        border='1px solid'
        borderColor='gray.300'
        borderRadius='8px'
        p={8}
        boxShadow='md'>
        <Box
          as='h3'
          textAlign='center'
          letterSpacing='wide'
          fontSize='2rem'
          fontWeight='light'
          mb={{base: 4}}>
          Sign in to{' '}
          <Box as='span' fontWeight='semibold' color='blue.400'>
            Xamer.
          </Box>
        </Box>
        <form onSubmit={onSubmit}>
          <FormControl isRequired>
            <FormLabel color='gray.500' htmlFor='username'>
              Username
            </FormLabel>
            <InputGroup>
              <InputLeftElement
                children={<Icon name='at-sign' color='gray.400' />}
              />
              <Input
                type='text'
                name='username'
                variant='filled'
                placeholder='Enter your username'
                value={username.trim()}
                onChange={onChange}
              />
            </InputGroup>
          </FormControl>
          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='password'>
              Password
            </FormLabel>
            <InputGroup>
              <InputLeftElement
                children={<Icon name='lock' color='gray.400' />}
              />
              <Input
                type={show ? 'text' : 'password'}
                name='password'
                variant='filled'
                value={password.trim()}
                placeholder='Password obviously'
                onChange={onChange}
              />
              <InputRightElement width='4.5rem'>
                <Button
                  h='1.75rem'
                  bg='gray.200'
                  size='sm'
                  onClick={() => setShow(!show)}>
                  {show ? 'Hide' : 'Show'}
                </Button>
              </InputRightElement>
            </InputGroup>
          </FormControl>
          <Button mt={4} variantColor='blue' type='submit'>
            Login
          </Button>
        </form>
        <Box mt='3' as='h4' lineHeight='tight'>
          New here?{' '}
          <Box as='span' fontWeight='semibold' color='blue.500'>
            <Link to='/register'>Register here</Link>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error: state.auth.error,
  isAuthenticated: state.auth.isAuthenticated,
  isFaculty: state.auth.isFaculty,
  isStudent: state.auth.isStudent,
});

export default connect(mapStateToProps, {loginUser, clearErrors})(Login);
