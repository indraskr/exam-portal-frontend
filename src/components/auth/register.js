import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {registerStudent, clearErrors} from '../../state/actions/authActions';
import BatchListOptions from '../batch/BatchListOptions';
import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Button,
  Input,
  InputGroup,
  InputRightElement,
  InputLeftElement,
  Icon,
  Select,
  useToast,
} from '@chakra-ui/core';

const Register = props => {
  const {registerStudent, clearErrors, error, isAuthenticated} = props;
  const [user, setUser] = useState({
    name: '',
    username: '',
    password: '',
    batchName: '',
  });
  const [show, setShow] = useState(false);

  const {username, password, name} = user;

  const onChange = e => setUser({...user, [e.target.name]: e.target.value});

  const toast = useToast();
  useEffect(() => {
    if (error !== null && error.id === 'USER_EXISTS') {
      toast({
        position: 'top-right',
        title: 'Error',
        description: error.msg,
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
      clearErrors();
    }
    if (error !== null && error.error.length > 1) {
      error.error.forEach(err => {
        toast({
          position: 'top-right',
          title: 'Error',
          description: err.msg,
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      });
      clearErrors();
    }
    if (isAuthenticated) {
      props.history.push('/');
    }
    // eslint-disable-next-line
  }, [error, props.history, isAuthenticated]);

  const onSubmit = async e => {
    e.preventDefault();
    if (username === '' || password === '') {
      console.log('Please fill out all required fields');
    } else {
      registerStudent(user);
    }
  };
  return (
    <Flex w='100%' h='100vh' align='center' justify='center' direction='column'>
      <Box
        w={{base: '90%', sm: '80%', md: '35%'}}
        border='1px solid'
        borderColor='gray.300'
        borderRadius='8px'
        p={8}
        boxShadow='md'>
        <Box
          as='h3'
          textAlign='center'
          letterSpacing='wide'
          fontSize='2rem'
          fontWeight='light'
          mb={{base: 4}}>
          Sign up to{' '}
          <Box as='span' fontWeight='semibold' color='blue.400'>
            Xamer.
          </Box>
        </Box>
        <form onSubmit={onSubmit}>
          <FormControl isRequired>
            <FormLabel color='gray.500' htmlFor='username'>
              Full name
            </FormLabel>
            <InputGroup>
              <InputLeftElement
                children={<Icon name='info' color='gray.400' />}
              />
              <Input
                type='text'
                name='name'
                placeholder='Enter your full name'
                variant='filled'
                value={name}
                onChange={onChange}
              />
            </InputGroup>
          </FormControl>

          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='username'>
              Username
            </FormLabel>
            <InputGroup>
              <InputLeftElement
                children={<Icon name='at-sign' color='gray.400' />}
              />
              <Input
                type='text'
                name='username'
                placeholder='Enter username'
                variant='filled'
                value={username.trim()}
                onChange={onChange}
              />
            </InputGroup>
          </FormControl>

          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='password'>
              Password
            </FormLabel>
            <InputGroup>
              <InputLeftElement
                children={<Icon name='lock' color='gray.400' />}
              />
              <Input
                type={show ? 'text' : 'password'}
                name='password'
                variant='filled'
                placeholder='Enter password'
                value={password.trim()}
                onChange={onChange}
              />
              <InputRightElement width='4.5rem'>
                <Button
                  h='1.75rem'
                  bg='gray.200'
                  size='sm'
                  onClick={() => setShow(!show)}>
                  {show ? 'Hide' : 'Show'}
                </Button>
              </InputRightElement>
            </InputGroup>
          </FormControl>

          <FormControl mt={2} isRequired>
            <FormLabel color='gray.500' htmlFor='batch'>
              Batch
            </FormLabel>
            <InputGroup>
              <Select
                icon='triangle-down'
                iconSize={3}
                variant='filled'
                id='batch'
                name='batchName'
                placeholder='Select your batch'
                onChange={onChange}>
                <BatchListOptions />
              </Select>
            </InputGroup>
          </FormControl>
          <Button mt={4} variantColor='blue' type='submit'>
            Sign up
          </Button>
        </form>
        <Box mt='3' as='h4' lineHeight='tight'>
          Existing user?{' '}
          <Box as='span' fontWeight='semibold' color='blue.500'>
            <Link to='/login'>Login here</Link>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};
const mapStateToProps = state => ({
  error: state.auth.error,
  isAuthenticated: state.auth.isAuthenticated,
});
export default connect(mapStateToProps, {registerStudent, clearErrors})(
  Register
);
