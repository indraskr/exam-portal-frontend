import {Box, Flex} from '@chakra-ui/core';
import React, {Suspense, useEffect} from 'react';
import {connect} from 'react-redux';
import {getBatches} from '../../state/actions/batchActions';
import {getAllTest} from '../../state/actions/testActions';
import {getStudents} from '../../state/actions/studentAction';
import Navbar from '../layouts/Navbar';
import Sidebar from '../layouts/Sidebar';
import {loadUser} from '../../state/actions/authActions';
import {getSubmissions} from '../../state/actions/submissionAction';

const Tests = React.lazy(() => import('../tests/Tests'));
const Submission = React.lazy(() => import('../submissions/Submission'));
const Batch = React.lazy(() => import('../batch/Batch'));
const Student = React.lazy(() => import('../students/Student'));
const CompletedTests = React.lazy(() => import('../tests/CompletedTests'));

const Faculty = ({
  mainContent,
  getAllTest,
  getBatches,
  getStudents,
  loadUser,
  getSubmissions,
}) => {
  const mainViews = {
    Tests: <Tests />,
    Submission: <Submission />,
    Batch: <Batch />,
    Student: <Student />,
    CompletedTests: <CompletedTests />,
  };

  useEffect(() => {
    loadUser();
    getAllTest();
    getBatches();
    getStudents();
    getSubmissions();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Navbar />
      <Flex w='full' minH='100vh'>
        <Box flex='2' bg='gray.900' display={{base: 'none', md: 'flex'}}>
          <Sidebar />
        </Box>
        <Box p='1rem' bg='gray.300' as='main' flex='7'>
          {/* TODO: set fallback to spinner */}
          <Suspense fallback={<div>Loading main content...</div>}>
            {mainViews[mainContent]}
          </Suspense>
        </Box>
      </Flex>
    </>
  );
};
const mapStateToProps = state => ({
  mainContent: state.ui.mainContent,
});
export default connect(mapStateToProps, {
  loadUser,
  getAllTest,
  getBatches,
  getStudents,
  getSubmissions,
})(Faculty);
