import React from 'react';
import {useLocation, Link} from 'react-router-dom';

const ErrorPage = () => {
  let location = useLocation();
  return (
    <div>
      <h1 style={{fontSize: '20rem'}}>404</h1>
      <h3>
        <code style={{fontSize: '2rem', color: 'red'}}>
          url:{location.pathname}
        </code>
      </h3>
      <Link to='/'>
        <button style={{background: 'grey', color: 'white', padding: '1rem'}}>
          GO HOME
        </button>
      </Link>
    </div>
  );
};

export default ErrorPage;
