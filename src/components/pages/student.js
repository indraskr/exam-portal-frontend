import React, {Suspense, useEffect} from 'react';
import Navbar from '../layouts/Navbar';
import {Box, Flex} from '@chakra-ui/core';
import Sidebar from '../layouts/Sidebar';
import {connect} from 'react-redux';
import {loadUser} from '../../state/actions/authActions';
import {getAllTest} from '../../state/actions/testActions';
const Tests = React.lazy(() => import('../tests/Tests'));
const CompletedTests = React.lazy(() => import('../tests/CompletedTests'));

const Student = ({loadUser, getAllTest, mainContent, user}) => {
  useEffect(() => {
    loadUser();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    user.student !== undefined && getAllTest(user.student.batch_id);
    // eslint-disable-next-line
  }, [user]);

  const mainViews = {
    Tests: <Tests />,
    CompletedTests: <CompletedTests />,
  };
  return (
    <>
      <Navbar />
      <Flex w='full' minH='100vh'>
        <Box flex='2' bg='gray.900' display={{base: 'none', md: 'flex'}}>
          <Sidebar />
        </Box>
        <Box p='1rem' bg='gray.300' as='main' flex='7'>
          {/* TODO: set fallback to spinner */}
          <Suspense fallback={<div>Loading main content...</div>}>
            {mainViews[mainContent]}
          </Suspense>
        </Box>
      </Flex>
    </>
  );
};

const mapStateToProps = state => ({
  mainContent: state.ui.mainContent,
  user: state.auth.user,
});

export default connect(mapStateToProps, {loadUser, getAllTest})(Student);
