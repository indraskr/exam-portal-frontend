import {Box, Button, Flex, Heading, Radio, RadioGroup} from '@chakra-ui/core';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {useParams} from 'react-router-dom';
import {addSubmission} from '../../state/actions/submissionAction';
import TestDetailsAccordion from '../tests/TestDetailsAccordion';

const TestTaker = ({tests, user, addSubmission, history, submission}) => {
  const {id} = useParams();
  const currentTest = tests.find(test => test._id === id);

  const {questions, test_details} = currentTest;

  // tracks left time
  const {testTimeHours, testTimeMinutes} = test_details;
  const [timeLeft, setTimeLeft] = useState(
    testTimeHours * 60 + testTimeMinutes
  );

  useEffect(() => {
    if (timeLeft === 0) onTestSubmit();
    let testTimer = setTimeout(() => setTimeLeft(timeLeft - 1), 60000);
    return () => {
      clearTimeout(testTimer);
    };
  }, [timeLeft]);

  const [answers, setAnswers] = useState([]);

  // handles option changes and answer updates
  const filterAnswers = newAns => {
    const found = answers.find(ans => ans.qsn_no === newAns.qsn_no);
    if (!found) setAnswers([...answers, newAns]);
    if (found)
      setAnswers(
        answers.map(ans => (ans.qsn_no === found.qsn_no ? newAns : ans))
      );
  };

  const onTestSubmit = () => {
    addSubmission({
      student_id: user.student._id,
      test_id: currentTest._id,
      faculty_id: currentTest.faculty_id,
      submitted_ans: answers,
    });
    // TODO: show confirmation if time left
    history.push('/');
  };

  const GenerateQuestions = () =>
    questions.map(qsn => (
      <Box px={3} key={qsn.qsn_no}>
        <Heading as='h1' size='lg'>
          {`${qsn.qsn_no}. ${qsn.question}`}
        </Heading>
        <Box marginX={3} marginY={6}>
          <RadioGroup
            onChange={e =>
              filterAnswers({qsn_no: qsn.qsn_no, ans: e.target.value})
            }>
            {Object.keys(qsn.options).map((key, idx) => (
              <Radio key={idx} borderColor='gray.400' value={key}>
                {qsn.options[key]}
              </Radio>
            ))}
          </RadioGroup>
        </Box>
      </Box>
    ));

  return (
    <Flex
      bg='gray.200'
      w='full'
      minH='100vh'
      justify='center'
      align='center'
      p={{base: 2, sm: 8}}>
      <Flex
        border='1px solid grey'
        flexDirection='column'
        p={6}
        w={{base: '100vw', sm: '95vw', md: '60vw'}}
        rounded='md'
        shadow='lg'>
        <TestDetailsAccordion testDetails={test_details} timeLeft={timeLeft} />
        {GenerateQuestions()}
        <Button
          ml={{sm: 0, md: 5}}
          w={{base: 'full', md: '5rem'}}
          onClick={onTestSubmit}
          variant='outline'
          variantColor='teal'>
          Submit
        </Button>
      </Flex>
    </Flex>
  );
};

const mapStateToProps = state => ({
  tests: state.test.tests,
  user: state.auth.user,
  submission: state.submission,
});

export default connect(mapStateToProps, {addSubmission})(TestTaker);
