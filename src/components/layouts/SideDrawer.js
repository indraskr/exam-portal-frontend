import {
  Drawer,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  useDisclosure,
  IconButton,
} from '@chakra-ui/core';
import React, {Fragment} from 'react';
import Sidebar from '../layouts/Sidebar';
import {AiOutlineMenu} from 'react-icons/ai';

const SideDrawer = () => {
  const {isOpen, onOpen, onClose} = useDisclosure();
  return (
    <Fragment>
      <IconButton
        onClick={onOpen}
        color='White'
        variant='ghost'
        _hover={{bg: '#0000'}}
        aria-label='drawer menu'
        fontSize='20px'
        mt={1}
        mr={2}
        ml={-3}
        display={{base: 'block', md: 'none'}}
        icon={AiOutlineMenu}
      />
      <Drawer size='xs' placement='left' onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent bg='gray.900'>
          <DrawerHeader
            color='gray.100'
            fontSize='1.5rem'
            borderBottomWidth='1px'
            borderColor='gray.600'>
            Xamer.
          </DrawerHeader>
          <Sidebar onClose={onClose} />
        </DrawerContent>
      </Drawer>
    </Fragment>
  );
};

export default SideDrawer;
