import React from 'react';
import {IconButton, Box, Tooltip} from '@chakra-ui/core';

const FAB = ({tooltipLabel, onClick}) => {
  return (
    <Box pos='fixed' bottom='1rem' right='1rem'>
      <Tooltip hasArrow label={tooltipLabel} placement='left'>
        <IconButton
          variantColor='blue'
          aria-label={tooltipLabel}
          size='lg'
          icon='add'
          rounded='full'
          onClick={onClick}
        />
      </Tooltip>
    </Box>
  );
};

export default FAB;
