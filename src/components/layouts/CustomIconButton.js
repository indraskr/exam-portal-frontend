import {Button} from '@chakra-ui/core';
import React from 'react';

const CustomIconButton = props => {
  return (
    <Button
      zIndex='1'
      shadow='lg'
      variant='solid'
      variantColor='blue'
      {...props}>
      {props.children}
    </Button>
  );
};

export default CustomIconButton;
