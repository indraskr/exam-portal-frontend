import {Box, List, ListIcon, ListItem} from '@chakra-ui/core';
import React from 'react';
import {
  AiOutlineCheckCircle,
  AiOutlineRead,
  AiOutlineSolution,
  AiOutlineTeam,
  AiOutlineFileDone,
} from 'react-icons/ai';
import {connect} from 'react-redux';
import {ChangeMainContent} from '../../state/actions/uiActions';

const Sidebar = ({ChangeMainContent, user, mainContent, onClose}) => {
  const isFaculty = user.isFaculty;

  const setMainContent = componentName => {
    ChangeMainContent(componentName);
    onClose && onClose();
  };

  const ListItemBuilder = (componentName, listName, ListContent, iconName) => (
    <ListItem
      _hover={{bg: 'gray.600'}}
      cursor='pointer'
      bg={
        mainContent === listName
          ? {background: '#2D3748'}
          : {background: '#0000'}
      }
      p='0.5rem 0.6rem'
      borderRadius={3}
      onClick={() => setMainContent(componentName, listName)}>
      <ListIcon
        icon={iconName}
        size='20px'
        mb={1}
        color={mainContent === listName ? 'blue.400' : 'gray.200'}
      />
      {ListContent}
    </ListItem>
  );

  return (
    <Box w='full' color='gray.200' className='sidebar' p='1.5rem .5rem'>
      <List spacing={1} w='100%'>
        {ListItemBuilder('Tests', 'Tests', 'All tests', AiOutlineRead)}
        {ListItemBuilder(
          'CompletedTests',
          'CompletedTests',
          'Completed tests',
          AiOutlineFileDone
        )}
        {isFaculty && (
          <>
            {ListItemBuilder(
              'Submission',
              'Submission',
              'Submissions',
              AiOutlineCheckCircle
            )}
            {ListItemBuilder('Batch', 'Batch', 'Batches', AiOutlineSolution)}
            {ListItemBuilder('Student', 'Student', 'Students', AiOutlineTeam)}
          </>
        )}
      </List>
    </Box>
  );
};

const mapStateToProps = state => ({
  mainContent: state.ui.mainContent,
  user: state.auth.user,
});

export default connect(mapStateToProps, {ChangeMainContent})(Sidebar);
