import {
  Button,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
} from '@chakra-ui/core';
import React from 'react';

const PopOverButton = ({headerMsg, bodyMsg, children, onClick}) => {
  return (
    <Popover placement='bottom-end'>
      <PopoverTrigger>{children}</PopoverTrigger>
      <PopoverContent zIndex={4}>
        <PopoverArrow />
        <PopoverCloseButton />
        <PopoverHeader fontWeight='bold' border='0'>
          {headerMsg}
        </PopoverHeader>
        <PopoverBody>{bodyMsg}</PopoverBody>
        <PopoverFooter
          border='0'
          d='flex'
          alignItems='center'
          justifyContent='flex-end'>
          <Button onClick={onClick} variantColor='blue'>
            Confirm
          </Button>
        </PopoverFooter>
      </PopoverContent>
    </Popover>
  );
};

export default PopOverButton;
