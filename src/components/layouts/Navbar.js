import {
  Avatar,
  Box,
  Flex,
  Heading,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
  Tag,
  TagLabel,
} from '@chakra-ui/core';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import SideDrawer from '../layouts/SideDrawer';
import {RiLogoutCircleRLine} from 'react-icons/ri';
import {logOut} from '../../state/actions/authActions';

const Navbar = ({auth, logOut}) => {
  const {user, isAuthenticated} = auth;
  const [userDetails, setUserDetails] = useState({name: 'User', avatarUrl: ''});

  useEffect(() => {
    if (user.isFaculty) setUserDetails({name: user.faculty.name});
    if (user.isStudent) setUserDetails({name: user.student.name});
  }, [user]);

  return (
    <Flex
      w='full'
      h='3rem'
      bg='gray.800'
      align='center'
      p={4}
      pos='sticky'
      top='0'
      zIndex='2'>
      <SideDrawer />
      <Heading as='h3' color='gray.100' size='lg'>
        Xamer.
      </Heading>
      <Box ml='auto'>
        {isAuthenticated && (
          <Box
            onClick={() => logOut()}
            display='inline-block'
            as={RiLogoutCircleRLine}
            size='22px'
            color='gray.200'
            mr={2}
            cursor='pointer'
            title='Logout'
          />
        )}
        <Menu>
          <MenuButton>
            <Tag bg='gray.700' variant='subtle' rounded='5px'>
              <TagLabel color='gray.200' mb={1}>
                {userDetails.name}
              </TagLabel>
              <Avatar
                src={userDetails.avatarUrl}
                size='xs'
                name={userDetails.name}
                ml={2}
              />
            </Tag>
          </MenuButton>
          <MenuList>
            <MenuGroup title='Profile'>
              <MenuItem>My Account</MenuItem>
              <MenuItem>Settings</MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuGroup title='Help'>
              <MenuItem>Docs</MenuItem>
              <MenuItem>FAQ</MenuItem>
            </MenuGroup>
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  );
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {logOut})(Navbar);
