import React from 'react';
import {Flex, Avatar, Text} from '@chakra-ui/core';
import {connect} from 'react-redux';

const StudentItem = ({studentName, batchID, batches}) => {
  const getBatchName = () =>
    batches.find(batch => batch._id === batchID).name.toUpperCase();

  return (
    <>
      <Flex
        cursor='pointer'
        bg='gray.200'
        shadow='md'
        _hover={{shadow: 'lg'}}
        borderRadius='md'
        flexDir='column'
        justify='center'
        align='center'
        p={3}
        w={{base: '8rem', sm: '10rem'}}
        m={3}>
        <Avatar mb={2} size='xl' name={studentName} src='' />
        <Text color='gray.800' fontSize={{base: 'md', sm: 'xl'}}>
          {studentName}
        </Text>
        <Text color='gray.600' fontSize={{base: 'sm', sm: 'md'}}>
          {getBatchName()}
        </Text>
      </Flex>
    </>
  );
};

const mapStateToProps = state => ({
  batches: state.batch.batches,
});

export default connect(mapStateToProps, {})(StudentItem);
