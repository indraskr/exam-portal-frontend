import {Flex} from '@chakra-ui/core';
import React from 'react';
import {connect} from 'react-redux';
import StudentItem from './StudentItem';

const Student = ({students}) => {
  return (
    <Flex w='full' wrap='wrap' justify={{base: 'center', sm: 'start'}}>
      {students.map(student => (
        <StudentItem
          key={student._id}
          batchID={student.batch_id}
          studentName={student.name}
        />
      ))}
    </Flex>
  );
};

const mapStateToProps = state => ({
  students: state.student.students,
});

export default connect(mapStateToProps, {})(Student);
